var express = require('express');
const { response } = require('../app');
const db = require('../db')
var router = express.Router();

const user1 = "user1"
const user2 = "user2"
const password = "password"

router.get('/', function(req, res, next) {
  if (req.session.ranjivost == undefined) {
    req.session.ranjivost = "omogućena"
  }
  if (req.session.ranjivost2 == undefined) {
    req.session.ranjivost2 = "omogućena"
  }
  if (req.session.login == undefined) {
    req.session.login = ""
  }
  res.render('home', {login:"", user:req.session.user, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/', function(req, res, next) {
  let token = generateToken()
  req.session.token = token
  if (req.session.ranjivost == undefined) {
    req.session.ranjivost = "omogućena"
  }
  if (req.session.ranjivost2 == undefined) {
    req.session.ranjivost2 = "omogućena"
  }
  if (req.session.login == undefined) {
    req.session.login = ""
  }

  if (req.body.username == user1 && req.body.password == password) {
    req.session.user = user1
  } else if (req.body.username == user2 && req.body.password == password){
    req.session.user = user2
  } else {
    req.session.login = "Invalid username or password. Try again!"
    res.render('home', {login:req.session.login, user:req.session.user, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
  }
  req.session.login = "Logged in as: " + req.session.user
  res.render('home', {login:req.session.login, user:req.session.user, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/enable', function(req, res, next) {
  res.render('home', {login:req.session.login, user:req.session.user, ranjivost:"omogućena", ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/disable', function(req, res, next) {
  res.render('home', {login:req.session.login, user:req.session.user, ranjivost:"onemogućena", ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/enable2', function(req, res, next) {
  res.render('home', {login:req.session.login, user:req.session.user, ranjivost:req.session.ranjivost, ranjivost2:"omogućena", token:req.session.token});
});

router.post('/disable2', function(req, res, next) {
  res.render('home', {login:req.session.login, user:req.session.user, ranjivost:req.session.ranjivost, ranjivost2:"onemogućena", token:req.session.token});
});

router.get('/sql-injection-enabled', function(req, res, next) {
  res.render('sql-injection', {songs:"", type:"enabled", user:req.session.user, login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/sql-injection-enabled', function(req, res, next) {
  (  async () => {
    if (req.body.artist != undefined) {
      var songs = await getSongs(req.body.artist)
      res.render('sql-injection', {
        songs: JSON.stringify(songs), 
        type:"enabled", user:req.session.user,
        login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2,
        token:req.session.token

      })
    }

    res.render('sql-injection', {
      songs:"", 
      type:"enabled", user:req.session.user,
      login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2,
      token:req.session.token
    })
  })()
});

router.get('/sql-injection-disabled', function(req, res, next) {
  res.render('sql-injection', {songs:"", type:"disabled", user:req.session.user,  login:req.session.login, 
  ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/sql-injection-disabled', function(req, res, next) {
  (  async () => {
    if (req.body.artist != undefined) {
      if (req.body.artist.includes(';') || req.body.artist.includes('\'') || req.body.artist.toLowerCase().includes(' union ')
       || req.body.artist.toLowerCase().includes(' or ') || req.body.artist.toLowerCase().includes(' delete ')
       || req.body.artist.toLowerCase().includes(' drop ') || req.body.artist.toLowerCase().includes(' select ')
       || req.body.artist.toLowerCase().includes(' from ') || req.body.artist.toLowerCase().includes(' where ')) {
        res.render('sql-injection', {
          songs:"Possible SQL injection attempt noticed!!!",
          type:"disabled", user:req.session.user,
          login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token
        })
      }

      var songs = await getSongs(req.body.artist)
      res.render('sql-injection', {
        songs: JSON.stringify(songs), 
        type:"disabled", user:req.session.user,
        login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token
      })
    }

    res.render('sql-injection', {
      songs:"", 
      type:"disabled", user:req.session.user,
      login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token
    })
  })()
});
router.get('/broken-access-control-enabled/user1', function(req, res, next) {
  res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Important information about user1",
  login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.get('/broken-access-control-enabled/user2', function(req, res, next) {
  res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Important information about user2",
  login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.get('/broken-access-control-disabled/user1', function(req, res, next) {
  if (req.session.user != user1) {
    res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Krivi korisnik. Zabranjen pristup.",
    login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
  }
  res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Important information about user1",
  login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.get('/broken-access-control-disabled/user2', function(req, res, next) {
  if (req.session.user != user2) {
    res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Krivi korisnik. Zabranjen pristup.",
    login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
  }
  res.render('broken-access-control', {songs:"", type:"enabled", user:req.session.user, info:"Important information about user2",
  login:req.session.login, ranjivost:req.session.ranjivost, ranjivost2:req.session.ranjivost2, token:req.session.token});
});

router.post('/csrf', function(req, res, next) {
  res.render('csrf', {user: req.session.user, session: req.session.id, login:req.session.login, 
    ranjivost:req.session.ranjivost, csrf:req.body.csrf, token:req.body.token})
})
router.post('/id', function(req, res, next) {
  console.log(req.body)
  console.log(req.session)
  if (req.body.csrf == "ne" && req.session.token != req.body.token) {
      res.send("TOKENS DO NOT MATCH!!!!");
  }
  res.send(req.session.id)
})


function generateToken() {
  return Math.random().toString(36).substr(2)
}


async function getSongs(artist) {
  const sql = `SELECT author, title, year FROM songs WHERE author = '` + artist + `'`;
  try {
    const result = await db.query(sql, []);
    return result.rows;
  } catch (err) {
    console.log(err);
    throw err
  }
}



module.exports = router;
