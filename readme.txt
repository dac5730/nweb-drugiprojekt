https://nweb-projekt2.herokuapp.com/

[SQL umetanje (SQL Injection), Loša kontrola pristupa (Broken Access Control), Lažiranje zahtjeva na drugom sjedištu (Cross Site Request Forgery, CSRF)]

Upute za isprobati aplikaciju su u navedene u samoj aplikaciji.
U slučaju lokalnog pokretanja, potrebno je prvo otvoriti index.js i seed.js iz db foldera, zakomentirati pool koji je služio za heroku db, i otkomentirati pool
koji služi za lokalnu bazu podataka. Zatim je potrebno napraviti novu bazu podataka zvanu '2projekt' i eventualno promjeniti user-a i password ako ih lokalno imate drugačije.
Potrebno je instalirati sve module, i to se napravi tako da budete pozicionirani u glavnom folderu i runate 'npm install'.
Nakon toga potrebno je pokrenuti seed.js tako da se pozicionirate u db folder i upišete u terminal komandu 'node seed.js'. 
Nakon toga iz glavnog foldera moežte runati 'npm start' i pokrenuti aplikaciju.
