const {Pool} = require('pg');

const pool = new Pool({
    user: 'eblxntbszxoaej',
    host: 'ec2-54-229-68-88.eu-west-1.compute.amazonaws.com',
    database: 'd7bg3ek4390tgh',
    password: '97a7ba50aea4d138fb54c51549e5f23af16a8b2d69972460fdbd50ca721d738a',
    port: 5432,
    ssl: {
	    rejectUnauthorized: false,
	}
});

/**
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: '2projekt',
    password: 'bazepodataka',
    port: 5432,
});
*/
module.exports = {
    query: (text, params) => {
        const start = Date.now();
        return pool.query(text, params)
            .then(res => {
                const duration = Date.now() - start;
                //console.log('executed query', {text, params, duration, rows: res.rows});
                return res;
            });
    },
    pool: pool
}
