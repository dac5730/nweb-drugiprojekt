const {Pool} = require('pg');

const pool = new Pool({
    user: 'eblxntbszxoaej',
    host: 'ec2-54-229-68-88.eu-west-1.compute.amazonaws.com',
    database: 'd7bg3ek4390tgh',
    password: '97a7ba50aea4d138fb54c51549e5f23af16a8b2d69972460fdbd50ca721d738a',
    port: 5432,
    ssl: {
	    rejectUnauthorized: false,
	}
});

/**
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: '2projekt',
    password: 'bazepodataka',
    port: 5432,
});
*/

const sql_create_users = `CREATE TABLE users (
    username text NOT NULL UNIQUE,
    password text NOT NULL,
	id serial,
	primary key(id)
)`;

const sql_create_songs = `CREATE TABLE songs (
    author text NOT NULL,
	title text NOT NULL,
    year numeric,
	id serial,
	primary key(id)
)`;

let table_names = [
    "users",
    "songs",
]

const sql_insert_users = `INSERT INTO users VALUES 
    ('Mirko', 'password123'),
    ('Slavko', '12345678'),
    ('Miroskav', 'asdfghjkl'),
    ('Branko', 'totalysafepasswordnobodywilleverhackit');
`;

const sql_insert_songs = `INSERT INTO songs VALUES 
    ('Kanye West', 'Runaway', 2010),
	('Kanye West', 'Power', 2010),
    ('Kanye West', 'Monster', 2010),
    ('Kanye West', 'Dark Fantasy', 2010),
    ('Kanye West', 'All of the lights', 2010),
    ('Kanye West', 'Hurricane', 2021),
    ('Kanye West', 'Off the grid', 2021),
	('Kendrick Lamar', 'Money Trees', 2012),
	('Kendrick Lamar', 'm.A.A.d city', 2012),
	('Kendrick Lamar', 'Backseet freestyle', 2012),
	('Kendrick Lamar', 'Swimming pools', 2012);
`;

let tables = [
    sql_create_users,
    sql_create_songs
];

let table_data = [
    sql_insert_users,
    sql_insert_songs
];

( async () => {
    for (let i = 0; i < tables.length; i++) {
        console.log("Creating table " + table_names[i]+ ".");
        try {
            await pool.query(tables[i], [])
            console.log("Table " + table_names[i] + " created.");
            if( table_data[i] !== undefined ) {
                try {
                    await pool.query(table_data[i], [])
                    console.log("Table " + table_names[i] + " populated with data.");
                }
                catch(err) {
                    console.log("Error populating table " + table_names[i] + " with data.")
                    return console.log(err.message);
                }
            }
        }
        catch(err) {
            console.log("Error creating table " + table_names[i])
            return console.log(err.message);
        }
    }
}) ()
